import {auth, database} from './firebase'

function getTasks(callback) {
    var tasks = [];

    database.ref(`/${auth.currentUser.uid}/tasks/`).once('value').then((snapshot) => {
        snapshot.forEach(snapshot => {
            tasks = [...tasks, snapshot.val()];
        });

        callback(tasks);
    });
}

function compare(a, b) {
    let comparison = 0;

    if (a.timeMillis > b.timeMillis) {
        comparison = 1;
    }
    else {
        comparison = -1;
    }
    return comparison;
}

class taskPlus {
    taskObject = null;

    constructor(object) {
        this.taskObject = object;
    }

    isLarge = null;
    timeMillis = null;
    isInProgress = false;
    isSelected = false;
}

/*
TODO:

-Need to make sure hitting the deadline is prioritized over trying to keep large stuff away from each other
xGet tasks ordered by due date and keep large stuff away
xGet tasks ordered by due date

 */

function sort() {
    getTasks((tasks) => {
        console.log("DB Raw:");
        for (let i = 0; i < tasks.length; i++) {
            console.log(i + " title: " + tasks[i].title + " requiredTime: " + tasks[i].requiredTime + " dueDate: " + tasks[i].dueDate)
        }

        var taskList = [];
        var taskListOrdered = [];
        var taskListFinal = [];
        for (let i = 0; i < tasks.length; i++) {
            taskList = [...taskList, new taskPlus(tasks[i])];
            if (taskList[i].taskObject.requiredTime >= 480) {
                taskList[i].isLarge = true;
            }
            else {
                taskList[i].isLarge = false;
            }
            taskList[i].timeMillis = (new Date(taskList[i].taskObject.dueDate)).getTime();
        }
        taskListOrdered = taskList;
        taskListOrdered.sort(compare);
        console.log("Due date ordered:");
        for (let i = 0; i < taskListOrdered.length; i++) {
            console.log(i + " title: " + taskListOrdered[i].taskObject.title + " requiredTime: " + taskListOrdered[i].taskObject.requiredTime + " dueDate: " + taskListOrdered[i].taskObject.dueDate)
        }
        let override = false;
        for (let i = 0; i < taskListOrdered.length; i++) {
            if (i === 0) {
                taskListFinal[i] = taskListOrdered[i];
                taskListOrdered[i].isSelected = true;
            }
            else {
                console.log(i + " Looping");
                //var override = false;
                for (let u = 0; u < taskListOrdered.length; u++) {
                    if (taskListFinal[i - 1] == null) {
                        window.alert("This should never happen. Oops.");
                    }
                    if (taskListFinal[i - 1].isLarge || override) {                    //If the previous task is large
                        if (!taskListOrdered[u].isLarge || override) {              //If the potential task is not large
                            if (!taskListOrdered[u].isSelected) {      //If the potential task has not already been selected.
                                taskListFinal[i] = taskListOrdered[u];
                                taskListOrdered[u].isSelected = true;
                                console.log("Index added is " + u);
                                break;
                            } else {
                                console.log(u + " Already selected before is big " + " Override: " + override);
                            }
                        }
                    } else {
                        if (!taskListOrdered[u].isSelected) {      //If the potential task has not already been selected.
                            taskListFinal[i] = taskListOrdered[u];
                            taskListOrdered[u].isSelected = true;
                            console.log("Index added is " + u);
                            break;
                        } else {
                            console.log(u + " Already selected Before is small");
                        }
                    }
                    if (u === (taskListOrdered.length - 1)) {
                        //window.alert("This should never happen. Oops.");
                        console.log("Welp, we're all out of ordered small tasks to put after this big task so we're just going to have to turn on the override and put some big ones after it.")
                        override = true;
                        i = i - 1
                        //break;
                    }
                }
            }

        }

        let events = [];

        let currentDate = new Date();
        currentDate.setHours(8);
        currentDate.setMinutes(0);
        let currentTime = currentDate.getTime();

        console.log("Final order:");
        for (let i = 0; i < taskListFinal.length; i++) {
            console.log(i + " title: " + taskListFinal[i].taskObject.title + " requiredTime: " + taskListFinal[i].taskObject.requiredTime + " dueDate: " + taskListFinal[i].taskObject.dueDate)

            let startDate = new Date(currentTime);

            if (startDate.getDay() === 6) {
                startDate.setDate(startDate.getDate()+2);
            } else if (startDate.getDay() === 7) {
                startDate.setDate(startDate.getDate()+1);
            }

            currentTime = startDate.getTime();

            let endTime = currentTime + (taskListFinal[i].taskObject.requiredTime*60*1000);
            let endDate = new Date(endTime);

            let dayEnd = new Date(currentTime);
            dayEnd.setHours(17);
            dayEnd.setMinutes(0);

            if ((endDate.getHours() > 17 && Math.abs((taskListFinal[i].taskObject.requiredTime*60*1000) - dayEnd.getTime()) > 60*60*1000) || endDate.getDate() > startDate.getDate()) {
                startDate.setHours(8);
                startDate.setMinutes(0);
                startDate.setDate(startDate.getDate()+1);
            }

            currentTime = startDate.getTime();
            endTime = currentTime + (taskListFinal[i].taskObject.requiredTime*60*1000);
            endDate = new Date(endTime);

            events = [...events, {
                title: taskListFinal[i].taskObject.title,
                start: startDate.toISOString(),
                end: endDate.toISOString()
            }];

            if (endDate.getHours() > 17) {
                endDate.setHours(8);
                endDate.setMinutes(0);
                endDate.setDate(endDate.getDate()+1);
            }

            currentTime = endDate.getTime();
        }
        console.log("Fina Order + start/stop times:");
        for (let i = 0; i < events.length; i++) {
            console.log(i + " title: " + events[i].title + " start: " + events[i].start + " end: " + events[i].end)
        }

        database.ref(`/${auth.currentUser.uid}/events/`).set(events);
    });
}

export default sort;