import React, {Component} from 'react';
import './index.css';
import {Redirect} from 'react-router-dom';
import * as auth from './auth';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

class CreateAccountPage extends Component {
	constructor(props) {
		super(props);

		this.state = {
			redirectToHome: false,
			email: "",
			password: ""
		};

		auth.addStateChangeListener(user => {
			this.setState({redirectToHome: !!user});
		});

		this.handleEmailChange = this.handleEmailChange.bind(this);
		this.handlePasswordChange = this.handlePasswordChange.bind(this);
	}

	signUp = () => {
		auth.createUser(this.state.email, this.state.password).then(() => {
			this.setState({ redirectToHome: true })
		});
	};

	handleEmailChange(event) {
		this.setState({email: event.target.value});
	}

	handlePasswordChange(event) {
		this.setState({password: event.target.value});
	}

	render() {
		const { redirectToHome } = this.state;

		if (redirectToHome) {
			return <Redirect to={{ pathname: "/" }} />;
		}

		return (
			<div className="App">
				<p>Create account</p>
				<form onSubmit={this.signUp}>
					<TextField type="text" placeholder="Email" value={this.state.email} onChange={this.handleEmailChange}/><br/>
					<TextField type="password" placeholder="Password" value={this.state.password} onChange={this.handlePasswordChange}/><br/>
					<Button variant={"outlined"} className="marginTop" type="submit">Create Account</Button>
				</form>
			</div>
		);
	}
}

export default CreateAccountPage;
