import React from 'react';
import ReactDOM from 'react-dom';
import {HashRouter as Router, Route, Link, withRouter, Redirect} from 'react-router-dom';
import './index.css';
import * as auth from './auth';
import HomePage from "./HomePage";
import TaskEditorPage from "./TaskEditorPage";
import LoginPage from "./LoginPage";
import CreateAccountPage from "./CreateAccountPage";
import registerServiceWorker from './registerServiceWorker';
const AuthButton = withRouter(({history}) => auth.isSignedIn() ? (
    <a onClick={() => auth.signOut().then(() => history.push("/login"))} className="navBarLink signOutButton" style={{position: "relative", top: "6px"}}>Logout</a>
) : (
    null
));

const PrivateRoute = ({component: Component, ...rest}) => (
    <Route
        {...rest}
        render={props =>
            auth.isSignedIn() ? (
                <Component {...props} />
            ) : (
                <Redirect
                    to={{
                        pathname: "/login",
                        state: {from: props.location}
                    }}
                />
            )
        }
    />
);

const PrivateLink = (props) => {
    return auth.isSignedIn() ? (
        <Link to={props.to} className={props.className}>{props.children}</Link>
    ) : (
        null
    )
};

const App = () => (
    <Router>
        <div>
            <header className="navBar">
                <Link to="/" className="navBarLink" style={{fontSize: "1.5em", fontWeight: "bold"}}>Task Scheduler</Link>
                {/*<PrivateLink to="/tasks" className="navBarLink">Tasks</PrivateLink>*/}
                <AuthButton/>
            </header>

            <PrivateRoute exact path="/" component={HomePage}/>
            <PrivateRoute path="/tasks" component={TaskEditorPage}/>
            <Route path="/login" component={LoginPage}/>
            <Route path="/signup" component={CreateAccountPage}/>
        </div>
    </Router>
);
ReactDOM.render(<App/>, document.getElementById('root'));
registerServiceWorker();
