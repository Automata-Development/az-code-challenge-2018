import React, { Component } from "react";
import Calendar from "react-big-calendar";
import moment from "moment";
import "./index.css";
import "react-big-calendar/lib/css/react-big-calendar.css";

Calendar.setLocalizer(Calendar.momentLocalizer(moment));

class EventCalendar extends Component {
    state = {
        events: [
            {
                start: new Date(2018, 8, 2, 0, 0, 0, 0),
                end: new Date(2018, 8, 8, 0, 0, 0, 0),
                title: "Some title stuff"
            }
        ]
    };

    render() {
        return (
            <div className="calendarContainer">
                <Calendar
                    defaultDate={new Date()}
                    defaultView="month"
                    events={this.state.events}
                />
            </div>
        );
    }
}

export default EventCalendar;