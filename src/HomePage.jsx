import React, {Component} from 'react';
import 'react-circular-progressbar/dist/styles.css';
import "react-big-calendar/lib/css/react-big-calendar.css";
import './index.css';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import CircularProgressbar from 'react-circular-progressbar';
import Modal from "react-modal";
import {auth, database} from "./firebase";
import TextField from "@material-ui/core/TextField/TextField";
import DateTimePicker from "react-datetime-picker";
import sort from './Algorithm';
import Calendar from "react-big-calendar";
import moment from "moment";

Calendar.setLocalizer(Calendar.momentLocalizer(moment));

const customStyles = {
    overlay: {zIndex: 1000},
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    }
};

class HomePage extends Component {
    constructor(props) {
        super(props);
        sort();
        this.state = {
            newItemTitle: "",
            newItemDueDate: new Date(),
            newItemRequiredMinutes: 0,
            newItemRequiredHours: 0,
            showModal: false,
            showEventModal: false,
            selectedEvent: null,
            events: [],
            percentage1: ((new Date()).getHours()/24)*100,
            percentage2: ((new Date()).getDay()/7)*100,
        };
    }

            
    componentWillMount() {
        database.ref(`/${auth.currentUser.uid}/events/`).on('value', (snapshot) => {
            this.setState({events: []});

            snapshot.forEach(snapshot => {
                let event = snapshot.val();

                this.setState(prevState => ({
                    events: [...prevState.events, {
                        start: new Date(event.start),
                        end: new Date(event.end),
                        title: event.title,
                    }]
                }));
            });

            this.setState({percentage1: (100 - ((this.getTimeLeftInDay()/600)*100))});
            console.log("TimeLeft: " + this.getTimeLeftInDay());
            this.setState({percentage2: (100 - (this.getTimeLeftInTask() * 100))});
            console.log("TaskTimeLeft: " + (100 - (this.getTimeLeftInTask() * 100)))
        });
    }

    getTimeLeftInDay = () => {
        let rightNow = new Date();
        for (let i = this.state.events.length - 1; i >= 0 ; i--) {
            if(this.state.events[i].end.getDate() == rightNow.getDate()) {
                return ((((this.state.events[i].end.getHours() * 60) + (this.state.events[i].end.getMinutes())) - ((rightNow.getHours() * 60) + (rightNow.getMinutes()))))
            }
        }
        return 0;
    };

    getTimeLeftInTask = () => {
        let rightNow = new Date();
        for (let i = 0; i < this.state.events.length ; i++) {
            if(this.state.events[i].start < rightNow) {
                if(this.state.events[i].end > rightNow) {
                    return ((((this.state.events[i].end.getHours() * 60) + (this.state.events[i].end.getMinutes())) - ((rightNow.getHours() * 60) + (rightNow.getMinutes())))/(((this.state.events[i].end.getHours() *60) + this.state.events[i].end.getMinutes()) - ((this.state.events[i].start.getHours() *60) + this.state.events[i].start.getMinutes())));
                }
            }
        }
        return 0;
    };

    renderEventModal = () => {
        return (this.state.selectedEvent ?
            <div>
                <p>{"Title: " + this.state.selectedEvent.title}</p>
                <p>{"Start: " + this.state.selectedEvent.start}</p>
                <p>{"End: " + this.state.selectedEvent.end}</p>
                <p>{"Duration: " + (this.state.selectedEvent.end.getHours() - this.state.selectedEvent.start.getHours()) + "H " + (this.state.selectedEvent.end.getMinutes() - this.state.selectedEvent.start.getMinutes()) + "M"}</p>
                <Button variant={"outlined"} className="submitButton" onClick={this.closeEventModal}>Done</Button>
            </div> : null
        );
    };

    renderModal = () => {
        return (
            <div>
                <form onSubmit={this.addTask}>
                    <TextField type="text" value={this.state.newItemTitle} placeholder="Title" className="marginBottom"
                               onChange={(event) => {
                                   this.setState({newItemTitle: event.target.value});
                               }}/>
                    <br/>
                    Time Required:
                    <br/>

                    <div className='inLineDiv marginTop marginBottom'>
                        Hours:
                        <input className='numberInput marginLeft marginRight' value={this.state.newItemRequiredHours} type="number"
                               onChange={(event) => {
                                   this.setState({newItemRequiredHours: event.target.value})
                               }}
                        />
                        Minutes:
                        <input className='numberInput marginLeft marginRight' value={this.state.newItemRequiredMinutes} type="number"
                               onChange={(event) => {
                                   this.setState({newItemRequiredMinutes: event.target.value})
                               }}
                        />
                    </div>
                    <br/>
                    <DateTimePicker
                        onChange={this.onNewItemDateChanged}
                        value={this.state.newItemDueDate}
                        className="marginBottom"/>
                    <br/>

                    <div className='inLineDiv marginBottom'>
                        <Button variant={"outlined"} type="submit" className="submitButton marginRight">Done</Button>
                        <Button variant={"outlined"} onClick={this.cancelAddTask}>Cancel</Button>
                    </div>
                </form>
            </div>
        );
    };

    addTask = () => {
        database.ref(`/${auth.currentUser.uid}/tasks/`).push({
            title: this.state.newItemTitle,
            dueDate: this.state.newItemDueDate.toISOString(),
            requiredTime: (parseInt(this.state.newItemRequiredHours, 10) * 60) + (parseInt(this.state.newItemRequiredMinutes, 10))
        });

        this.setState({
            newItemTitle: "",
            newItemDueDate: new Date(),
            newItemRequiredMinutes: 0,
            newItemRequiredHours: 0,
            showModal: false,
        });

        sort();
    };

    cancelAddTask = () => {
        this.setState({
            newItemTitle: "",
            newItemDueDate: new Date(),
            newItemRequiredMinutes: 0,
            newItemRequiredHours: 0,
            showModal: false,
        });
    };

    onNewItemDateChanged = (date) => {
        this.setState({newItemDueDate: date});
    };

    showModal = () => {
        this.setState({
            showModal: true
        });
    };

    closeModal = () => {
        this.setState({
            showModal: false
        });
    };
    
    showEventModal = (event) => {
        this.setState({
            showEventModal: true,
            selectedEvent: event,
        });
    };

    closeEventModal = () => {
        this.setState({
            showEventModal: false,
        });
    };

    render() {
        return (
            <div className="App flexColumn">
                {/*<header className="App-header">
                    <h1 className="App-title">Dashboard</h1>
                </header>*/}

                <div className="flexRow" style={{height: "200px", margin: "20px"}}>
                    <CircularProgressbar
                        percentage={this.state.percentage1}
                        text={"Day"}/>

                    <CircularProgressbar
                        percentage={this.state.percentage2}
                        text={"Task"}/>

                    <div style={{width: "56px", height: "56px"}}>
                        <Button variant="fab"
                                color="primary"
                                aria-label="Add"
                                style={{position: "relative", top: "144px"}}
                                onClick={this.showModal}>
                            <AddIcon/>
                        </Button>
                    </div>
                </div>

                <div className="calendarContainer">
                    <Calendar
                        selectable
                        defaultDate={new Date()}
                        defaultView="month"
                        events={this.state.events}
                        onSelectEvent={event => this.showEventModal(event)}
                    />
                </div>

                <Modal isOpen={this.state.showModal}
                       onRequestClose={this.closeModal}
                       style={customStyles}>
                    {this.renderModal()}
                </Modal>

                <Modal isOpen={this.state.showEventModal}
                       onRequestClose={this.closeEventModal}
                       style={customStyles}>
                    {this.renderEventModal()}
                </Modal>
            </div>
        )
    }
}

export default HomePage;
