import firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';

var config = {
    apiKey: "AIzaSyA3aDZ_DAeC2Ya0WDAeXTzD5S4I5FxXhqU",
    authDomain: "az-code-challenge-2018.firebaseapp.com",
    databaseURL: "https://az-code-challenge-2018.firebaseio.com",
    projectId: "az-code-challenge-2018",
    storageBucket: "az-code-challenge-2018.appspot.com",
    messagingSenderId: "86665373966"
};

if (!firebase.apps.length) {
    firebase.initializeApp(config);
}

const database = firebase.database();
const auth = firebase.auth();

export {
    database,
    auth,
};
