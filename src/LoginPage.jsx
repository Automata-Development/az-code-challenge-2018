import React, {Component} from 'react';
import './index.css';
import {Redirect, withRouter} from 'react-router-dom';
import * as auth from './auth';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

const CreateAccountButton = withRouter(({ history }) => (
	<Button variant={"outlined"} className="marginTop" onClick={() => history.push('/signup')}>Create Account</Button>
));

class LoginPage extends Component {
	constructor(props) {
		super(props);

		this.state = {
			redirectToReferrer: false,
			email: "",
			password: "",
			errorText: "",
            showErrorText: false
		};

		this.handleEmailChange = this.handleEmailChange.bind(this);
		this.handlePasswordChange = this.handlePasswordChange.bind(this);
	}

	login = () => {
		auth.signIn(this.state.email, this.state.password).then(() => {
			auth.addStateChangeListener(user => {
				this.setState({redirectToReferrer: true});
			});
		}).catch(() => {
            this.setState({errorText: "Error signing in", showErrorText: true});
		});
	};

	handleEmailChange(event) {
		this.setState({email: event.target.value});
	}

	handlePasswordChange(event) {
		this.setState({password: event.target.value});
	}

	render() {
		const { from } = this.props.location.state || { from: { pathname: "/" } };
		const { redirectToReferrer } = this.state;

		if (redirectToReferrer) {
			return <Redirect to={from} />;
		}

		return (
			<div className="App">
				<p>You must log in to view this page.</p>
				<form onSubmit={this.login}>
					<TextField type="text" placeholder="Email" value={this.state.email} onChange={this.handleEmailChange}/><br/>
					<TextField type="password" placeholder="Password" value={this.state.password} onChange={this.handlePasswordChange}/><br/>
					<Button variant={"outlined"} className="marginTop" type="submit">Login</Button>
				</form>

				<CreateAccountButton/>

                {this.state.showErrorText ? (
                    <p>{this.state.errorText}</p>
                ) : null}
			</div>
		);
	}
}

export default LoginPage;
