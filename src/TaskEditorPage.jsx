import React, {Component} from 'react';
import './index.css';
import {database} from './firebase'
import Modal from 'react-modal'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Icon from '@material-ui/core/Icon';
import {auth} from './firebase';
import DateTimePicker from 'react-datetime-picker';
class Picker extends Component {
    state = {
        date: new Date(),
    }

    onChange = date => this.setState({ date })

    render() {
        return (
            <div>
                <DateTimePicker
                    onChange={this.onChange}
                    value={this.state.date}
                />
            </div>
        );
    }
}
class TaskEditorPage extends Component {

    constructor(props) {
        super(props);

        this.state = {items: [], showModal: false, currentItem: 0};
    }

    componentWillMount() {
        database.ref(`/${auth.currentUser.uid}/tasks/`).once('value').then((snapshot) => {
            let items = snapshot.val();
            for (const index in items) {
                let item = items[index];
                this.setState(prevState => ({
                    items: [...prevState.items, item]
                }));
            }
        });
    }

    handleItemEdited = (newValue, index) => {
        let data = this.state.items;
        data[index] = newValue;
        this.setState({items: data});
    };

    handleModalSubmitted = () => {
        let item = this.state.items[this.state.currentItem];

        this.handleItemEdited(item, this.state.currentItem);

        database.ref(`/${auth.currentUser.uid}/tasks/${this.state.currentItem}/`).set(this.state.items[this.state.currentItem]);

        this.closeModal();
    };

    renderListItem = (item, id) => {
        return (
            <tr key={id}>
                <td><p className="linkItemText" onClick={() => this.editItem(id)}>{item.title}</p></td>
                <td><Icon className="rearrangeIcon" onClick={() => this.editItem(id)}>edit</Icon></td>
                <td><Icon className="rearrangeIcon" onClick={() => this.removeItem(id)}>delete</Icon></td>

                <td>
                    <Icon className="rearrangeIcon" onClick={() => this.moveItemUp(id)}>arrow_upward</Icon>
                    <Icon className="rearrangeIcon" onClick={() => this.moveItemDown(id)}>arrow_downward</Icon>
                </td>
            </tr>
        );
    };

    renderModal = () => {
        let item = this.state.items[this.state.currentItem];

        if (item) {
            return (
                <div>
                    <form onSubmit={this.handleModalSubmitted}>
                            item.title = event.target.value;

                            this.handleItemEdited(item, this.state.currentItem);
                        }}/>
                        <br/>

                        {/*<TextField type="text" value={item.timeRequired} placeholder="Time Required" className="wide" onChange={(event) => {
                            item.timeRequired = event.target.value;

                            this.handleItemEdited(item, this.state.currentItem);
                        }}/>*/}
                        Time Required:
                        <br/>

                        <div className='inLineDiv'>
                            Hours:
                            <input className='numberInput' value={item.timeRequired} type="number"/>
                            Minutes:
                            <input className='numberInput' value={item.timeRequired} type="number"/>
                        </div>

{/*
                        <TextField type="text" value={item.dueDate} placeholder="Due Date" className="wide" onChange={(event) => {
                            item.dueDate = event.target.value;

                            this.handleItemEdited(item, this.state.currentItem);
                        }}/>
*/}
                        <Picker />
                        <br/>

                        <Button variant={"outlined"} type="submit">Done</Button>
                    </form>
                </div>
            )
        } else {
            return null
        }
    };

    addItem = () => {
        let newItem = {
            title: "",
            timeRequired: "",
            dueDate: "",
        };

        this.setState(prevState => ({
            items: [...prevState.items, newItem]
        }), () => {
            this.editItem(this.state.items.length-1);
        });
    };

    removeItem = (index) => {
        let array = [...this.state.items];
        array.splice(index, 1);
        this.setState({items: array}, () => {
            database.ref(`/${auth.currentUser.uid}/tasks/`).set(this.state.items);
        });
    };

    editItem = (index) => {
        this.setState({
            currentItem: index
        });

        this.showModal();
    };

    moveItemUp = (index) => {
        if (index > 0) {
            let array = [...this.state.items];

            let temp = array[index];
            array[index] = array[index - 1];
            array[index - 1] = temp;

            this.setState({items: array}, () => {
                database.ref(`/${auth.currentUser.uid}/tasks/`).set(this.state.items);
            });
        }
    };

    moveItemDown = (index) => {
        if (index < this.state.items.length-1) {
            let array = [...this.state.items];

            let temp = array[index];
            array[index] = array[index + 1];
            array[index + 1] = temp;

            this.setState({items: array}, () => {
                database.ref(`/${auth.currentUser.uid}/tasks/`).set(this.state.items);
            });
        }
    };

    showModal = () => {
        this.setState({
            showModal: true
        });
    };

    closeModal = () => {
        this.setState({
            showModal: false
        });
    };

    render() {
        return (
            <div className="App">
                <Button variant={"outlined"} className="marginTop" onClick={this.addItem}>Add item</Button>


                <table>
                    <tr>
                        <th>Title</th>
                        <th>Edit</th>
                        <th>Delete</th>
                        <th>Move</th>
                    </tr>
                    {this.state.items.map(this.renderListItem)}
                </table>

                <Modal isOpen={this.state.showModal} onRequestClose={this.closeModal}>
                    {this.renderModal()}
                </Modal>
            </div>
        );
    }
}

export default TaskEditorPage;
